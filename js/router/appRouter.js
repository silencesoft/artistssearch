var app = app || {};

var AppRouter = Backbone.Router.extend({
	routes: {
		"artist/:name": "getArtist",
	},
	getArtist: function (name) {
		$('#form').addClass('search__form--results');
		$('#search').val(name);
		app.AppView.load(name);
	}
});
app.AppRouter = new AppRouter;
Backbone.history.start();
