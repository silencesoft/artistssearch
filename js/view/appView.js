var app = app || {};
var $apiURL = 'http://ws.audioscrobbler.com/2.0/?method=artist.getInfo&artist=%s&api_key=2b35547bd5675d8ecb2b911ee9901f59&format=json';
var $similarURL = 'http://ws.audioscrobbler.com/2.0/?method=artist.getSimilar&artist=%s&limit=12&api_key=2b35547bd5675d8ecb2b911ee9901f59&format=json';
var $songsURL = 'http://api.deezer.com/search/artist/?output=jsonp&q=%s';
 $songsURL = 'http://api.deezer.com/search?limit=5&q=artist:\'%s\'';

var AppView = Backbone.View.extend({
	el: $('#content'),
	initialize: function() {
		_.bindAll(this, 'render');
		/*
		this.collection.fetch({
		success: function () {
			that.render();
		}
		});
		*/
	},
	load: function (name) {
		var that = this;
		var $url = $apiURL.replace('%s', name);
		$.ajax({
			url: $url,
 			success:function(result){
				$('#content').html('');
				that.artist = new app.Artist(result.artist);
				// that.collection = new app.ArtistsCollection(result.artist.similar.artist);
				// that.render();
				that.loadSimilar(name);
		        // $('.mdl-spinner').removeClass('is-active');
				that.renderArtist(that.artist);
			}
		});
	},
	loadSimilar: function (name) {
		var that = this;
		var $url = $similarURL.replace('%s', name);
		$.ajax({
			url: $url,
			success:function(result){
				console.log(result);
				that.collection = new app.ArtistsCollection(result.similarartists.artist);
				// that.render();
				that.renderCollection(that.collection);
				that.loadSongs(name);
			}
		});
	},
	loadSongs: function (name) {
		var that = this;
		var $url = $songsURL.replace('%s', name);
		$.ajax({
			url: $url,
			dataType: "jsonp",
		    jsonp: "callback",
		    data: {
				output: 'jsonp'
			},
			success:function(result){
				console.log(result.data);
				that.songsCollection = new app.SongsCollection(result.data);
				// that.render();
				that.renderSongCollection(that.songsCollection);
		        $('.mdl-spinner').removeClass('is-active');
			}
		});
	},
	render: function () {
		var that = this;
		// this.renderArtist(this.artist);
		// this.renderCollection(this.collection);
		// this.renderSongCollection(this.songsCollection);
		/* _.each(this.collection.models, function (item) {
			that.renderItem(item);
		}, this); */
	},
	renderCollection: function (collection) {
		var artistsView = new app.ArtistsView({
			collection: collection
		});
		this.$el.append(artistsView.render().el);
	},
	renderSongCollection: function (collection) {
		var songsView = new app.SongsView({
			collection: collection
		});
		// songsView.$el.append(songsView.render().el);
		this.$('.content__artist__songs').html(songsView.render().el);
	},
	renderItem: function (item) {
		var artistsView = new app.ArtistsView({
			model: item
		});
		this.$el.append(artistsView.render().el);
	},
	renderArtist: function (item) {
		var artistView = new app.ArtistView({
		model: item
		});
		this.$el.append(artistView.render().el);
	}
});
app.AppView = new AppView();