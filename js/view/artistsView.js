var app = app || {};

app.ArtistsView = Backbone.View.extend({
		tagName: "artist",
		className: "artist-item",
		template: Handlebars.compile($('#artistsTemplate').html()), /* $("#artistsTemplate").html(), */

		render: function () {
			// var tmpl = _.template(this.template);
			// $(this.el).html(tmpl(this.model.toJSON()));
			// $(this.el).find( ".link" ).attr( "href", "#/artist/" + encodeURIComponent(this.model.get('name')) );
        this.$el.html(this.template({entries:this.collection.toJSON()}));
			console.log(this);
			return this;
		}
	});
