var app = app || {};

app.SongsView = Backbone.View.extend({
		tagName: "song",
		className: "song-item",
		template: Handlebars.compile($('#songsTemplate').html()), /* $("#artistsTemplate").html(), */

		render: function () {
			// var tmpl = _.template(this.template);
			// $(this.el).html(tmpl(this.model.toJSON()));
			// $(this.el).find( ".link" ).attr( "href", "#/artist/" + encodeURIComponent(this.model.get('name')) );
        	this.$el.html(this.template({entries:this.collection.toJSON()}));
			console.log(this.$el.html());
			return this;
		}
	});
