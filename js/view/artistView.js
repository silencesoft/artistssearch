var app = app || {};

app.ArtistView = Backbone.View.extend({
		tagName: "current",
		className: "current-artist-item",
		template: Handlebars.compile($('#artistTemplate').html()), /*$("#artistTemplate").html(),*/

		render: function () {
			// var tmpl = _.template(this.template);
			// $(this.el).html(tmpl(this.model.toJSON()));
			console.log(this.model.toJSON());
	        this.$el.html(this.template({item:this.model.toJSON()}));
			return this;
		}
	});
