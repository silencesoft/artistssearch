var app = app || {};

var FormView = Backbone.View.extend({
	el: $('body'),
	initialize: function () {
        // console.log("initialize");
    },
    events: {
		'submit form': 'submitForm',
		'keyup': 'processKey'
    }, 
    submitForm: function(e) {
        e.preventDefault();
        $('.mdl-spinner').addClass('is-active');
        var val = $('#search').val();
        if (val != '')
        	app.AppRouter.navigate('/artist/'+encodeURIComponent(val), true)
    } , 
    processKey: function(e) {
    	if(e.which === 13) // enter key
			this.$el.submit();
    }
});
app.FormView = new FormView();